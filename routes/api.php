<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function() {

    Route::post('profile', 'API\UserController@profile');

    Route::resource('users', 'API\UserController');

    // get rides but post as to post token
    Route::post('rides', 'API\RidesController@rides');
    Route::post('myrides', 'API\RidesController@myRides');

    // Post new ride
    Route::post('newride', 'API\RidesController@newRide');

    // Delete a ride
    Route::delete('ride/{rideId}', 'API\RidesController@deleteRide');

});
