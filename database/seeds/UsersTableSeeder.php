<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Zia',
            'email' => 'zia@gmail.com',
            'password' => bcrypt('zia'),
            'image' => 2,
        ]);
        DB::table('users')->insert([
            'name' => 'Ida',
            'email' => 'user2@email.com',
            'password' => bcrypt('password'),
            'image' => 1,
        ]);
    }
}
