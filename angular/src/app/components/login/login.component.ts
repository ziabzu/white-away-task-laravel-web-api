import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string
  password: string 

  constructor(private _userService: UserService, private _router: Router) { }

  ngOnInit() {
  }

  login() {

    console.log(this.email, this.password)

    let data = {
      'email': this.email,
      'password': this.password
    }

    this._userService.login(data).then(response => {

      localStorage.setItem('api-token', response.success.token)
      
      localStorage.setItem('user-id', response.success.user.id)
      localStorage.setItem('user-email', response.success.user.email)
      localStorage.setItem('user-name', response.success.user.name)

      this._userService.updateLoggedIn(true)
      this._router.navigate(['/'])

    })
    
  }

}
