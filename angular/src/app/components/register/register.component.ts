import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  name: string
  email: string
  password: string 
  c_password: string

  constructor(private _userService: UserService, private _router: Router) { }

  ngOnInit() {
  }

  register() {

    console.log(this.email, this.password)

    let data = {
      'name': this.name,
      'email': this.email,
      'password': this.password,
      'c_password': this.c_password
    }

    this._userService.register(data).then(response => {

      console.log('response', response)
      localStorage.setItem('api-token', response.success.token)

      localStorage.setItem('user-id', response.success.user.id)
      localStorage.setItem('user-email', response.success.user.email)
      localStorage.setItem('user-name', response.success.user.name)

      this._userService.updateLoggedIn(true)
      this._router.navigate(['/'])

    })
    
  }
}
