import { Component, OnInit } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { RideService } from 'src/app/services/ride.service';
import { HelperService } from 'src/app/services/helper.service';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-ride',
  templateUrl: './ride.component.html',
  styleUrls: ['./ride.component.scss']
})

export class RideComponent implements OnInit {

  ngOnInit(): void {
    this.getRides();
  }

  rides: any
  myRides: any

  constructor(private alerts: AlertsService, private http: HttpClient,private rideService: RideService, public helperService: HelperService) { }

  getRides() {

    this.rideService.getRides().then(response => {

      this.rides = response.success

    })

  }

  deleteRide(rideId) {

      console.log('Delet Ride' + rideId)

      this.rideService.deleteRide(rideId).then(response => {

        this.myRides = response.success

      })

  }

  newRide() {

    this.alerts.setMessage('Configurations saved successfully!','success');

    this.rideService.newRide().then(response => {

      console.log(response)

      alert('A ride fropm Copenhage to Arhus created automatically');

    })

  }

  getMyRides() {

    this.rideService.getMyRides().then(response => {

      this.myRides = response.success

    })

  }

}
