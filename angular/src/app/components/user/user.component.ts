import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: any

  constructor(private _userService: UserService) { }

  ngOnInit() {

    this.getProfile();

  }

  /**
   * Update Profile
   * Using HTTP verb 'PUT'
   */
  updateProfile() {

    console.log('this.user', this.user)

    let data = {
      '_method': 'patch',
      'name': this.user.name,
      'email': this.user.email,
      'password': this.user.password,
    }
    
    const payload = new HttpParams()
      .set('_method', 'patch')
      .set('username', this.user.name)
      .set('email', this.user.email)
      .set('password', this.user.password);

    this._userService.updateProfile(data).then(response => {

      console.log('get Profile()....', response)
      this.user = response.success;

    })

  }
  
  /**
   * get profile data
  */
  getProfile() {

    this._userService.profile().then(response => {

      this.user = response.success;

    })
    
  }
}
