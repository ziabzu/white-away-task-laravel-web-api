import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  login: boolean = false;

  
  title = 'Travel talk';
  
  constructor(private _http: HttpClient, private _userService: UserService, private _router: Router) {
    
  }
  
  ngOnInit(): void {

    // Check if its logged in or not
    
    this._userService.profile().then(response => {

      this.login = (!!response.success) ? true : false;

      console.log('login:', this.login)

    })

    this._userService.user$.subscribe((response) => {

      this.login = (!!response.success) ? true : false;

    });
  }

  logout() {
    
    console.log('clicked logout')
    this._userService.logout();

    this._router.navigate(['/'])

  }

}
