import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

// Import your library
import { AlertsModule } from 'angular-alert-module';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { RideComponent } from './components/rides/ride.component';

// Services
import { RideService } from 'src/app/services/ride.service';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    RideComponent,
    UserComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AlertsModule.forRoot()
  ],
  providers: [
    RideService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
