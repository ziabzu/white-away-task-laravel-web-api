import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  public apiBaseUrl = "http://localhost/travel_talk/public/api/";
  public imageBaseUrl = "http://localhost/travel_talk/public/images/";

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('api-token')
    })
  }

  public httpOptionsPatch = {
    headers: new HttpHeaders({
      'Content-Type':  'multipart/form-data',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('api-token')
    })
  }

  constructor() { }

  saveToken(token:string) {
    localStorage.setItem('api-token', token)
  }
  
  getToken() {
    localStorage.getItem('api-token')
  }

  getHttpHeader(type :string = '') {

    if(type == 'patch') {
      return {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('api-token')
        })
      }
    }

    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('api-token')
      })
    }
  }

}
