import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = "http://localhost/travel_talk/public/api/";

  // Observable String Source
  private userSource = new Subject<Object>();

  // Observable String Stream
  user$ = this.userSource.asObservable();

  isLogged: boolean = false;

  constructor(private http:HttpClient, private _helperService: HelperService) {}

  /**
   * 
   * @param data  {
   * 'email': email,
   * 'password': password
   * }
   * used to login in scucess case return response as promise
   */
  login(data): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'login', data).toPromise().then(response => {

        console.log('login response', response)
        // this.userSource.next(response)
        return response

    })

  }

 /**
   * 
   * @param data  {
   * 'email': email,
   * 'password': password,
   * 'c_password': string
   * }
   * used to register in scucess case return response as promise
   */
  register(data): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'register', data).toPromise().then(response => {

        console.log('register response', response)
        // this.userSource.next(response)
        return response

    })

  }

  /**
   * 
   * used to get profile data
   */
  updateProfile(data): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'users/5', data, this._helperService.getHttpHeader('patch')).toPromise().then(response => {

        console.log('profile detail response', response)
        // this.userSource.next(response)
        return response

    })

  }

  /**
   * 
   * used to get profile data
   */
  profile(): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'profile', null, this._helperService.getHttpHeader()).toPromise().then(response => {

        console.log('profile detail response', response)
        if(!!response.success)
          this.isLogged = true

        return response

    })

  }

  /**
   * 
   * @param loggedIn boolean falg set
   */
  logout() {

    localStorage.removeItem('api-token')
    this.isLogged = false;

    this.updateLoggedIn(false)

  }
  /**
   * 
   * @param loggedIn boolean falg set
   */
  updateLoggedIn(loggedIn) {

    this.isLogged = loggedIn;
    this.userSource.next({'success': loggedIn})

  }


}
