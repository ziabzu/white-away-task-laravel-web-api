import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Subject } from 'rxjs';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})

export class RideService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIxMTkxMTFjMWY1ZGI5Yjc3ZjQ1M2E5YzRmMDBjZjRjZDk4MTg0ZTNjNDIxY2JlYTQ2MWRiZTIxMTVmYmQ3MjMwMGQ0YzE3NTc2YjY4N2E5In0.eyJhdWQiOiIxIiwianRpIjoiMjExOTExMWMxZjVkYjliNzdmNDUzYTljNGYwMGNmNGNkOTgxODRlM2M0MjFjYmVhNDYxZGJlMjExNWZiZDcyMzAwZDRjMTc1NzZiNjg3YTkiLCJpYXQiOjE1NDgzMTM4MTEsIm5iZiI6MTU0ODMxMzgxMSwiZXhwIjoxNTc5ODQ5ODExLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.MhEZyWKETSUKo0ll33gG86Hl7h5cWQNIAWFPu-M5hhoZqUE1z4VvD--oqLwotT_vr7kvI17_nbm-xqXqAw6n4M_J8Dsxe0BrqT6eqi5KVdusihk3PwrY9P4L8Kheoy0kmJN9iK79E6Zyf_aw4Zl6_CDyUzBjWB-Bpb7YQGXDymBZ4nydzaj06bPBX-RUXVVG0yFNm_MXtZ8690fQjl5MK-2FeXiLLBnpX4WEY0thXwgk9yRvIyLsXkYSBRfE5Og1oKQpQ-SloKS_fr7xDJsKKewSgk0iJyXReZk7sc3vHRLsBZRxK9FZJPn2a7c8wuVH7qqBux66Na4o0YxNHvWFBaMNKHAJUx8pdprRqOyViLZH1C40kjJsHe2KML3Jlq3F3IOA2DZQVLzRVRxWoc6oOagnyNpTb4_oGoOb01Z72Cla3p64DRZszT4x6nGw72fu5I-rH8e-TnxoOUKLsYWIYYy14uqbWUsr3j2l-zBMf3ug87QEVf-8Q-LdNoR_N97X_Y7UrQQgy96IsbidO7WDPqddKIioqEbJqXHSNOsF3pV2BowKWGyv0-vDspbeZFjyxNLlzk6-BCRR5PdgO3nc3Xe3NL6OfpATpESn4vDXEBDpJLfUtWa_ntMX4UvlFxDLr-Wk0m2kv3Kb4SRRjch1YmWdNQAcD2U5Efu1ngMTt1s'
    })
  }

  // Observable String Source
  private rideSource = new Subject<Object>();

  // Observable String Stream
  ride$ = this.rideSource.asObservable();

  constructor(private http:HttpClient, private _helperService: HelperService) {}

  getRides(): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'rides', null, this._helperService.getHttpHeader()).toPromise().then(response => {

        this.rideSource.next(response)
        return response

    })

  }

  deleteRide(rideId: number):  Promise<any> {

    return this.http.delete(this._helperService.apiBaseUrl + 'ride/' + rideId, this._helperService.getHttpHeader()).toPromise().then(response => {

        this.rideSource.next(response)
        return response

    })

  }

  newRide():  Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'newride', null, this._helperService.getHttpHeader()).toPromise().then(response => {

        this.rideSource.next(response)
        return response

    })

  }

  getMyRides(): Promise<any> {

    return this.http.post(this._helperService.apiBaseUrl + 'myrides', null, this._helperService.getHttpHeader()).toPromise().then(response => {

        this.rideSource.next(response)
        return response

    })

  }

  updateRides(rides) {

    this.rideSource.next(rides)

  }


  updateMyRides(rides) {

    this.rideSource.next(rides)

  }

}
