import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RideComponent } from './components/rides/ride.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';

const routes: Routes = [
    {
      path: '',
      component: RideComponent
    },
    {
      path: 'profile',
      component: UserComponent
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
