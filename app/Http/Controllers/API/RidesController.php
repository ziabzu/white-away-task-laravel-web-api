<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
// use Validator;


use App\Ride;

class RidesController extends Controller
{

    public $successStatus = 200, $failureStatus = 401;

    /**
     * Rides api
     *  fetch all rides or user specific rides
     *
     * @return \Illuminate\Http\Response
     */
    public function rides(Request $request) {

        // $user = Auth::user();
        $rides = Ride::with('user')->get();
        return response()->json(['success' => $rides], $this->successStatus);

    }


    /**
     * Rides api
     *  fetch all rides or user specific rides
     *
     * @return \Illuminate\Http\Response
     */
    public function myRides(Request $request) {

        // $user = Auth::user();
        $rides = Ride::where('user_id', Auth::user()->id)->get();

        return response()->json(['success' => $rides], $this->successStatus);

    }


    /**
     * Rides api
     * Post new ride for user have token
     *
     * @return \Illuminate\Http\Response
     */
    public function newRide(Request $request) {

        $ride = new Ride;

        $ride->user_id =  Auth::user()->id;
        $ride->from =  'Copenhagen';
        $ride->to =  'Århus';
        $ride->no_of_seats = 3;
        $ride->drive_at = now();

        $ride->save();

        return response()->json(['success' => $ride], $this->successStatus);

    }

    /**
     * Rides api
     *  delete Ride only for specific user
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteRide(Request $request, $id) {

        $res = Ride::where([
            ['id', '=', $id],
            ['user_id', '=', Auth::user()->id],
        ])->delete();

        $rides = Ride::where('user_id', Auth::user()->id)->get();


        return response()->json(['success' => $id], $this->successStatus);

    }
}
