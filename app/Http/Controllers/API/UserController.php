<?php
namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Input; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

use Validator;

class UserController extends Controller
{

    public $successStatus = 200, $failureStatus = 401;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login() {

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;

            $success['user']['id'] = $user->id; 
            $success['user']['name'] = $user->name; 
            $success['user']['email'] = $user->email; 

            return response()->json(['success' => $success, $this->successStatus]);

        } else {

            return response()->json(['error' => 'Unauthorised'], $this->failureStatus);

        }

    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) return response()->json(['error'=>$validator->errors()], $this->failureStatus);

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;

        $success['user']['id'] = $user->id; 
        $success['user']['name'] = $user->name; 
        $success['user']['email'] = $user->email; 

        return response()->json(['success' => $success], $this->successStatus);

    }

    /**
     * Profile api
     *
     * @return \Illuminate\Http\Response
     */
    public function profile() {

        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);

    }

    /**
     * http verbs Patch
     */
    public function update(Request $request, $id) {

        $user = Auth::user();

        if(isset($request->name)) $user->name = $request->name;
        if(isset($request->email)) $user->email = $request->email;
        if(isset($request->password)) $user->password = $request->password;

        $user->save();

        return response()->json(['success' => $user, 'name' => $request->name], $this->successStatus);

    }

}
