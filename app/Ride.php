<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    
    // Inverse of one to many relationship 
    // Get the user rides belong to
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
